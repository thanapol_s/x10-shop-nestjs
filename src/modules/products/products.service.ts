import { Injectable, HttpStatus } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { MESSAGE } from "src/constants/message";
import { ENV } from "src/env/env";
import { IResponse } from "src/interfaces/type-response";
import { gen_uuid } from "src/shared";
import { returnResponse } from "src/shared/return-response";
import { Repository } from "typeorm";
import { CreateProductDto } from "./dto/create-product.dto";
import { ImageEntity } from "./entities/image.entity";
import { ProductEntity } from "./entities/product.entity";
import * as fs from 'fs';
import { UpdateProductDto } from "./dto/update-product.dto";


@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(ProductEntity)
    private readonly productRepo: Repository<ProductEntity>,
    @InjectRepository(ImageEntity)
    private readonly imageRepo: Repository<ImageEntity>,
  ) { }
  async create(body: CreateProductDto, files: Array<Express.Multer.File>): Promise<IResponse> {

    try {
      const product = new ProductEntity();
      product.p_name = body.p_name
      product.p_price = body.p_price
      product.p_color = body.p_color
      product.p_type = body.p_type
      product.p_description = body.p_description
      product.p_size = body.p_size
      product.p_status = body.p_status

      const saveProduct = await this.productRepo.save(product);
      console.log(saveProduct)
      if (saveProduct) {
        if (files != undefined) {
          for (const image of files) {
            console.log(image)
            const iamge_path = `${gen_uuid().toString()}.${image.mimetype.split('/')[1]}`;

            fs.writeFileSync(`./files/` + iamge_path, image.buffer);
            const productImage = new ImageEntity();
            productImage.image_name = gen_uuid().toString();
            productImage.image_type = image.mimetype.split('/')[1];
            productImage.image_url = ENV().URL_API + iamge_path;
            productImage.product = saveProduct

            await this.imageRepo.save(productImage);
          }
        }
        return returnResponse(true, HttpStatus.OK, MESSAGE.SUCCESS)
      } else {
        return returnResponse(false, HttpStatus.BAD_REQUEST, MESSAGE.FAILED)
      }
    } catch (error) {
      console.log(error)
      return returnResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, MESSAGE.NETWORK_ERROR)
    }


  }

  async findAll(): Promise<IResponse> {
    try {
      const findAll = await this.productRepo.find({
        relations: { file: true }, order: {
          id: "ASC"
        }
      })
      return returnResponse(true, HttpStatus.OK, MESSAGE.SUCCESS, findAll)
    } catch (error) {
      return returnResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, MESSAGE.NETWORK_ERROR)
    }
  }

  async findOne(id: string): Promise<IResponse> {
    try {
      const findOne = await this.productRepo.find({ where: { id } })
      if (findOne.length > 0) {
        return returnResponse(true, HttpStatus.OK, MESSAGE.SUCCESS, findOne)
      } else {
        return returnResponse(true, HttpStatus.OK, MESSAGE.FAILED, findOne)

      }
    } catch (error) {
      return returnResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, MESSAGE.NETWORK_ERROR)
    }
  }

  async update(id: string, body: UpdateProductDto, files: Array<Express.Multer.File>): Promise<IResponse> {
    try {
      const { fileDelete, ...result } = body
      const deleteFile = JSON.parse(fileDelete)
      for (const item of deleteFile) {
        await this.imageRepo.delete(item.id);
      }
      await this.productRepo.update(id, result);
      if (files != undefined) {
        const findOne = await this.productRepo.findOne({ where: { id } })
        for (const image of files) {
          console.log(image)
          const iamge_path = `${gen_uuid().toString()}.${image.mimetype.split('/')[1]}`;
          fs.writeFileSync(`./files/` + iamge_path, image.buffer);
          const productImage = new ImageEntity();
          productImage.image_name = gen_uuid().toString();
          productImage.image_type = image.mimetype.split('/')[1];
          productImage.image_url = ENV().URL_API + iamge_path;
          productImage.product = findOne

          await this.imageRepo.save(productImage);
        }
      }
      return returnResponse(true, HttpStatus.OK, MESSAGE.SUCCESS)
    } catch (error) {
      return returnResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, MESSAGE.NETWORK_ERROR, error)
    }
  }

  async remove(id: string): Promise<IResponse> {
    try {
      const findId = await this.imageRepo.find({ where: { product: { id } } })
      if (findId.length > 0) {
        findId.forEach(async (element) => {
          await this.imageRepo.delete(element.id);
        })
      }
      await this.productRepo.delete(id);
      return returnResponse(true, HttpStatus.OK, MESSAGE.SUCCESS)
    } catch (error) {
      return returnResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, MESSAGE.NETWORK_ERROR, error)
    }
  }
}
