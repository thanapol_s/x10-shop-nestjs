import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFiles } from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { ApiConsumes, ApiTags } from '@nestjs/swagger';
import { FilesInterceptor } from '@nestjs/platform-express';
import { IResponse } from 'src/interfaces/type-response';
import { UpdateProductDto } from './dto/update-product.dto';

@ApiTags("Product | ข้อมูลสินค้า")
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) { }

  @Post('create')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('files', 10))
  create(
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Body() body: CreateProductDto): Promise<IResponse> {
    return this.productsService.create(body, files);
  }

  @Get()
  findAll(): Promise<IResponse> {
    return this.productsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<IResponse> {
    return this.productsService.findOne(id);
  }

  @Patch(':id')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('files', 10))
  update(
    @Param('id') id: string, 
    @Body() body: UpdateProductDto, 
    @UploadedFiles() files: Array<Express.Multer.File>,): Promise<IResponse> {
    return this.productsService.update(id, body,files);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<IResponse> {
    return this.productsService.remove(id);
  }
}
