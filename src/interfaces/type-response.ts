export interface IResponse {
    result: boolean;
    status: number;
    message: String;
    data?: any;
    erro?: any;
    [key: string]: any;
}