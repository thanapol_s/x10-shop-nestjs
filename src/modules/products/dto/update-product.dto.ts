import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { CreateProductDto } from './create-product.dto';
import { ImageProductDto } from './image-product.dto';

export class UpdateProductDto {
    id?: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'p_name' })
    p_name: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'p_type' })
    p_type: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'p_price' })
    p_price: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'p_color' })
    p_color: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'p_size' })
    p_size: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'p_status' })
    p_status: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'p_description' })
    p_description: string;

    @ApiProperty({ type: [ImageProductDto],required: false})
    fileDelete?: string;

    @ApiPropertyOptional({
        required: false,
        type: 'array',
        items: { type: 'file' },
    })
    files?: Array<Express.Multer.File>;
}
