import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, Matches } from "class-validator";

export class CreateUserDto {

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'username' })
    username: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'password' })
    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, {
        message: "Password must be Minimum eight characters, at least one letter and one number"
    })
    password: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'firstname' })
    firstname: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'lastname' })
    lastname: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'email' })
    email: string;

}
