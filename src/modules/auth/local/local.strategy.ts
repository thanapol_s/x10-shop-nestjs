
import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { HttpStatus, Injectable } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { IResponse } from 'src/interfaces/type-response';
import { MESSAGE } from 'src/constants/message';
import { returnResponse } from 'src/shared/return-response';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
      super();
  }

  async validate(username: string, password: string): Promise<IResponse> {
    const user = await this.authService.validateUser(username, password);
    if (!user) {
        return returnResponse(false, HttpStatus.UNAUTHORIZED, MESSAGE.UNAUTHORIZED)
    }
    return user;
  }
}