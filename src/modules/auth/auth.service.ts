import { HttpStatus, Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { IResponse } from 'src/interfaces/type-response';
import { returnResponse } from 'src/shared/return-response';
import { MESSAGE } from 'src/constants/message';
import { ENV } from 'src/env/env';

@Injectable()
export class AuthService {

    constructor(
        private userService: UserService,
        private jwtService: JwtService
    ) { }

    async validateUser(username: string, password: string): Promise<any> {
        const user = await this.userService.findOneUser(username)
        if (user && await bcrypt.compare(password, user.password)) {
            const { password, ...result } = user
            return result
        }
        return null
    }

    async login(user: any): Promise<IResponse> {
        try {
            const payload = { username: user.username, sub: user.id };
            return returnResponse(true, HttpStatus.OK, MESSAGE.SUCCESS,{access_token:this.jwtService.sign(payload)})
        } catch (error) {
            return returnResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, MESSAGE.NETWORK_ERROR, null, error)
        }

    }
}
