import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IResponse } from 'src/interfaces/type-response';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
import { returnResponse } from 'src/shared/return-response';
import { MESSAGE } from 'src/constants/message';

@Injectable()
export class UserService {

  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepo: Repository<UserEntity>,
  ) { }

  async signUp(createUserDto: CreateUserDto): Promise<IResponse> {
    const { password, ...result } = createUserDto
    const hashedPassword = await bcrypt.hash(password, 10);
    try {
      const user = new UserEntity();
      user.username = result.username
      user.password = hashedPassword
      user.firstname = result.firstname
      user.lastname = result.lastname
      user.email = result.email

      await this.userRepo.save(user);
      return returnResponse(true, HttpStatus.OK, MESSAGE.SUCCESS)
    } catch (error) {
      console.log(error)
      return returnResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, MESSAGE.NETWORK_ERROR)
    }

  }
  create(createUserDto: CreateUserDto) {
    return 'This action adds a new user';
  }

  findAll() {
    return `This action returns all user`;
  }

  async findOneUser(username: string): Promise<UserEntity | undefined> {
    const user = await this.userRepo.findOne({ where: {username} })
    return user
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
