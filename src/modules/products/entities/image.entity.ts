import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { ProductEntity } from "./product.entity";

@Entity('product_image')
export class ImageEntity {
    @PrimaryGeneratedColumn() id:string;

    @Column({ type: 'varchar', nullable: false })
    image_name: string;

    @Column({ type: 'varchar', nullable: false })
    image_type: string;

    @Column({ type: 'varchar', nullable: false })
    image_url: string;

    @ManyToOne(()=> ProductEntity, image => image.file)
    product: ProductEntity

    @CreateDateColumn() createOn?: Date;
    @CreateDateColumn() updateOn?: Date;
}