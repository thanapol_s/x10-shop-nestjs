import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class ImageProductDto {
    @IsNotEmpty()
    @ApiProperty({ type: String})
    id: string;
}
