import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class LoginDto {
    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'test' })
    username: string;

    @IsNotEmpty()
    @ApiProperty({ type: String, example: 'test1234' })
    password: string;
}
